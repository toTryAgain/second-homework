public class Dog extends Animal {

    public Dog(String a) {
        super(a);
    }
    public String specificAction() {
        System.out.println(getName() + " ");
        decreaseFatigueIndex(10,25);
        decreaseHealth(5,10);
        decreaseHunger(2,5);
        return "бегает на улице...";

    }
}

