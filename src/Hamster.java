public class Hamster extends Animal {

    public Hamster(String a) {
        super(a);
    }
    public String specificAction() {
        System.out.println(getName() + " ");
        decreaseFatigueIndex(10,25);
        decreaseHealth(5,15);
        decreaseHunger(2,5);
        return "бегает в колесе...";
    }
}
