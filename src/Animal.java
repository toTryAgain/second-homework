public abstract class Animal {
   private int fatigueIndex;
   private String name;
   private int health;
   private int hunger;


    Animal (String a){
        fatigueIndex = 80;
        health = 80;
        hunger = 80;
        name = a;
    }

    public int getFatigueIndex(){
        return fatigueIndex;
    }

    public int getHealth(){
        return health;
    }

    public String getName() {
        return name;
    }

    public int getHunger() {
        return hunger;
    }

    public void decreaseFatigueIndex (int min, int max) {
        int n = max - min;
        fatigueIndex -= (Math.random() * n) + min;
    }

    public void increaseFatigueIndex (int min, int max) {
        int n = max - min;
        fatigueIndex += (Math.random() * n) + min;
    }

    public void decreaseHealth (int min, int max) {
        int n = max - min;
        health -= (Math.random() * n) + min;
    }

    public void increaseHealth (int min, int max) {
        int n = max - min;
        health += (Math.random() * n) + min;
    }

    public void increaseHunger (int min, int max) {
        int n = max - min;
        hunger += Math.random() *n + min;
    }

    public void decreaseHunger (int min, int max) {
        int n = max - min;
        hunger -= Math.random() * n + min;
    }


    public void sleep(){
        if (fatigueIndex == 100) {
            System.out.println("Не хочу спать...");
        }

            increaseFatigueIndex(10,30);
            increaseHealth(5,10);
            decreaseHunger(5,10);
            if (fatigueIndex > 100) {
                fatigueIndex -= fatigueIndex % 100;
    }

    }
     public void eat(){
         if (hunger >= 100) {
             System.out.println("Не хочу есть...");
         }else {
             increaseFatigueIndex(10, 30);
             increaseHealth(5, 10);
             increaseHunger(5, 10);
             if (hunger > 100) {
                 hunger -= hunger % 100;
             }
             System.out.println("Спасибо хохяин");
         }
         }


     public abstract String specificAction();
}




