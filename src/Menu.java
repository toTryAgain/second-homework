import java.util.Scanner;

public class Menu {

    Animal selectAnimal() {
        System.out.println("Выберите животное:");
        System.out.println("1.Кот");
        System.out.println("2.Собака");
        System.out.println("3.Хомяк");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        switch (a) {
            case 1: return new Cat("Мурзик");
            case 2: return new Dog("Шарик");
            case 3: return new Hamster("Жора");
            default:
                System.out.println("Неверный выбор");
        }
        return null;
    }

    void actionSelection (Animal myAnimal){
        System.out.println("Выберите действие:");
        System.out.println("Оставшаяся энергия " + myAnimal.getHunger() + "%");
        System.out.println("1.Есть");
        System.out.println("2.Спать");
        System.out.println("3.Выполнить специфическое действие");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        switch (a){
            case 1:
                myAnimal.eat();
                break;
            case 2: myAnimal.sleep();
                System.out.println("ZZZZzzz");
                break;
            case 3:
                System.out.println(myAnimal.getName() + " " + myAnimal.specificAction());
                break;

            default:
                System.out.println("Неверный выбор");
        }
    }
}
