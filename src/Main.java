public class Main {

    public static void main(String[] args) {
        Menu menu = new Menu();
        Animal myAnimal = menu.selectAnimal();
        System.out.println("Вы выбрали - " + myAnimal.getName());

        if (myAnimal.getFatigueIndex() < 25){
            System.out.println("Выш питомец устал, дайте ему отдохнуть! ");
        }


        while (myAnimal.getFatigueIndex() > 0) {
            menu.actionSelection(myAnimal);

        }

        System.out.println(myAnimal.getName() + "заболел, его увезут в больницу!");
    }
}
